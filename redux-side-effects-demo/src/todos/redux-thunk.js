import { createStore, applyMiddleware, bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import thunk from 'redux-thunk';

import { actionTypes, todosReducer } from './redux-common';
import { renderToDos, } from './components';

export const refreshToDos = () => {
  return dispatch => {
    dispatch({ type: actionTypes.REFRESH_TODOS_REQUEST });
    return fetch('http://localhost:3040/todos')
      .then(res => res.json())
      .then(todos => dispatch({ type: actionTypes.REFRESH_TODOS_FULFILLED, todos }));
  };
}

export const addToDo = todo => {
  return dispatch => {
    dispatch({ type: actionTypes.ADD_TODO_REQUEST, todo });
    return fetch('http://localhost:3040/todos', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(todo),
    }).then(() => dispatch(refreshToDos()));
  };
};

export const removeToDo = todoId => {
  return dispatch => {
    dispatch({ type: actionTypes.REMOVE_TODO_REQUEST, todo: null });
    return fetch('http://localhost:3040/todos/' + encodeURIComponent(todoId), {
      method: 'DELETE',
    }).then(() => dispatch(refreshToDos()));
  };
};

export const toggleToDoCompleted = todo => {
  return dispatch => {
    dispatch({ type: actionTypes.TOGGLE_TODO_COMPLETED_REQUEST, todo });
    return fetch('http://localhost:3040/todos/' + encodeURIComponent(todo.id), {
      method: 'PATCH',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ completed: !todo.completed }),
    }).then(() => dispatch(refreshToDos()));
  };
};


export const connectToDos = connect(
  ({ todos }) => ({ todos }),
  dispatch => bindActionCreators({
    refreshToDos,
    toggleToDoCompleted,
    removeToDo,
    addToDo
  }, dispatch)
);

const store = createStore(todosReducer, applyMiddleware(thunk));
renderToDos(connectToDos, store);

