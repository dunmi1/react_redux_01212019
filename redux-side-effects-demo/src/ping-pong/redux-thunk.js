import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

import { ping, pong, pingReducer } from './redux-common';

const pingThunk = () => {

  return dispatch => {
    dispatch(ping());
    const delay = new Promise(resolve => setTimeout(() => resolve(), 1000));
    return delay.then(() => {
      dispatch(pong());
    });
  };
};

const store = createStore(
  pingReducer,
  applyMiddleware(thunk)
);

store.subscribe(() => {
  console.log('thunk:', store.getState());
});

store.dispatch(pingThunk());