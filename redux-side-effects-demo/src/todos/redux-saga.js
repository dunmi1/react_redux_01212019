import { createStore, applyMiddleware, bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import createSagaMiddleware from 'redux-saga';
import { call, put, takeEvery, all, fork, takeLatest } from 'redux-saga/effects';

import { actionTypes, todosReducer } from './redux-common';
import { renderToDos, } from './components';


function* refreshToDosSaga() {
  console.log('waiting to refresh todos saga');
  yield takeLatest(actionTypes.REFRESH_TODOS_REQUEST, function* () {
    console.log('refreshing todos');
    const todos = yield call(() =>
      fetch('http://localhost:3040/todos').then(res => res.json()));
    yield put({ type: actionTypes.REFRESH_TODOS_FULFILLED, todos });
  });
}

function* addToDoSaga() {
  console.log('waiting to add todo saga');
  yield takeEvery(actionTypes.ADD_TODO_REQUEST, function* ({ todo }) {
    
    console.log('adding todo');

    const postToDo = () => fetch('http://localhost:3040/todos', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(todo),
    })
      .then(res => res.json());

    yield call(postToDo);
    yield put({ type: actionTypes.REFRESH_TODOS_REQUEST });
  });
}

function* removeToDoSaga() {
  yield takeEvery(actionTypes.REMOVE_TODO_REQUEST, function* ({ todoId }) {

    const deleteToDo = () => fetch('http://localhost:3040/todos/' + encodeURIComponent(todoId), {
      method: 'DELETE',
    })
      .then(res => res.json());

    yield call(deleteToDo);
    yield put({ type: actionTypes.REFRESH_TODOS_REQUEST });
  });
}

function* toggleCompletedToDoSaga() {
  yield takeEvery(actionTypes.TOGGLE_TODO_COMPLETED_REQUEST, function* ({ todo }) {

    const patchToDo = () => fetch('http://localhost:3040/todos/' + encodeURIComponent(todo.id), {
      method: 'PATCH',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ completed: !todo.completed }),      
    })
      .then(res => res.json());

    yield call(patchToDo);
    yield put({ type: actionTypes.REFRESH_TODOS_REQUEST });
  });
}


export const connectToDos = connect(
  ({ todos }) => ({ todos }),
  dispatch => bindActionCreators({
    refreshToDos: () => ({ type: actionTypes.REFRESH_TODOS_REQUEST }),
    toggleToDoCompleted: todo => ({ type: actionTypes.TOGGLE_TODO_COMPLETED_REQUEST, todo }),
    removeToDo: todoId => ({ type: actionTypes.REMOVE_TODO_REQUEST, todoId }),
    addToDo: todo => ({ type: actionTypes.ADD_TODO_REQUEST, todo })
  }, dispatch)
);

const sagaMiddleware = createSagaMiddleware();

const store = createStore(todosReducer, applyMiddleware(sagaMiddleware));

sagaMiddleware.run(function*() {
  yield all([
    fork(refreshToDosSaga),
    fork(addToDoSaga),
    fork(removeToDoSaga),
    fork(toggleCompletedToDoSaga),
  ]);
});

// sagaMiddleware.run(refreshToDosSaga);
// sagaMiddleware.run(addToDoSaga);
// sagaMiddleware.run(removeToDoSaga);
// sagaMiddleware.run(toggleCompletedToDoSaga);

renderToDos(connectToDos, store);

