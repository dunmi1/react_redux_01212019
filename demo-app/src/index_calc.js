import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import { calcStore } from './calcStore';
import { CalcToolContainer } from './containers/CalcToolContainer';


// const createStore = (reducerFn) => {

//   let currentState = undefined;
//   const subscribers = [];

//   return {
//     getState: () => currentState,
//     dispatch: (action) => {
//       currentState = reducerFn(currentState, action);
//       subscribers.forEach(cb => cb());
//     },
//     subscribe: (cb) => {
//       subscribers.push(cb);
//     },
//   };

// };

// const bindActionCreators = (actions, dispatch) => {
//   return Object.keys(actions).reduce( (boundActions, actionKey) => {
//     boundActions[actionKey] = (...params) => dispatch(actions[actionKey](...params));
//     return boundActions;
//   }, {});
// };

// const { Provider, Consumer } = React.createContext(null);

// const connect = (mapStateToPropsFn, mapDispatchToPropFn) => {

//   return PresentationalComponent => {

//     class ContainerComponent extends React.Component {

//       constructor(props) {
//         super(props);
//         this.dispatchProps = mapDispatchToPropFn(props.store.dispatch);
//       }

//       componentDidMount() {
//         this.storeUnsubscribe = this.props.store.subscribe(() => {
//           this.forceUpdate();
//         });
//       }

//       componentWillUnmount() {
//         this.storeUnsubscribe();
//       }

//       render() {
//         return <PresentationalComponent {...this.dispatchProps} {...mapStateToPropsFn(this.props.store.getState())} />;
//       }
//     }

//     const ContainerComponentContextWrapper = (props) =>
//       <Consumer>{value => <ContainerComponent {...props} store={value} />}</Consumer>;
    
//     return ContainerComponentContextWrapper;
//   };
// };



// ReactDOM.render(
//   <React.StrictMode>
//     <Provider store={calcStore}>
//       <CalcToolContainer />
//     </Provider>
//   </React.StrictMode>,
//   document.querySelector('#root')
// );


