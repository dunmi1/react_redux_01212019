import React, { useRef, useEffect } from 'react';

export const CalcTool = React.memo((props) => {

  const { onAdd, onSubtract, onMultiply, onDivide } = props;
  const { result, history } = props;

  const numInputRef = useRef(null); // React.createRef();

  useEffect(() => {
    numInputRef.current.focus();
  });

  const getNumInput = () => Number(numInputRef.current.value);

  return <form>
    <div>Result: {result}</div>
    <div>Input: <input type="number" defaultValue="0" ref={numInputRef} /></div>
    <button type="button" onClick={() => onAdd(getNumInput())}>Add</button>
    <button type="button" onClick={() => onSubtract(getNumInput())}>Subtract</button>
    <button type="button" onClick={() => onMultiply(getNumInput())}>Multiply</button>
    <button type="button" onClick={() => onDivide(getNumInput())}>Divide</button>
    <ul>
      {history.map( (item, i) => <li key={i}>{item}</li> )}
    </ul>
  </form>;

});