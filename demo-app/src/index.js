import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

// named import
// import { HelloWorld } from './components/HelloWorld';
// import { ColourTool } from './components/ColourTool';
// import { CarTool } from './components/CarTool';
import { CarToolContainer } from './components/CarToolContainer';
import { carStore } from './carStore';

// const colours = [
//   { name: 'black', hexCode: '#000000' },
//   { name: 'red', hexCode: '#FF0000' },
//   { name: 'green', hexCode: '#00FF00' },
// ];


ReactDOM.render(
  <Provider store={carStore}>
    <CarToolContainer />
  </Provider>,
  document.querySelector('#root')
);

