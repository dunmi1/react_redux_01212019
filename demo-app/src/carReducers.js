import {
  CAR_CANCEL_ACTION, CAR_EDIT_ACTION,
} from './carActions/carActions';

import { REFRESH_CARS_REQUEST, REFRESH_CARS_DONE } from './carActions/refreshCars';
import { APPEND_CAR_REQUEST } from './carActions/appendCar';
import { REPLACE_CAR_REQUEST } from './carActions/replaceCar';
import { DELETE_CAR_REQUEST } from './carActions/deleteCar';

export const carsReducer = (state = [], action) => {

  switch(action.type) {
    case REFRESH_CARS_DONE:
      return action.payload;
    default:
      return state;
  }

};

export const editCarIdReducer = (state = -1, action) => {

  switch(action.type) {
    case REFRESH_CARS_REQUEST:
    case APPEND_CAR_REQUEST:
    case REPLACE_CAR_REQUEST:
    case DELETE_CAR_REQUEST:
    case CAR_CANCEL_ACTION:
      return -1;
    case CAR_EDIT_ACTION:
      return action.payload;
    default:
      return state;
  }
};