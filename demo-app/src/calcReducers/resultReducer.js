import {
  CALC_ADD_ACTION, CALC_DIVIDE_ACTION,
  CALC_MULTIPLY_ACTION, CALC_SUBTRACT_ACTION
} from '../calcActions/calcActions';

export const resultReducer = (state = 0, action) => {

  switch (action.type) {
    case CALC_ADD_ACTION:
      return state + action.payload
    case CALC_SUBTRACT_ACTION:
      return state - action.payload
    case CALC_MULTIPLY_ACTION:
      return state * action.payload
    case CALC_DIVIDE_ACTION:
      return state / action.payload
    default:
      return state;
  }
  
}