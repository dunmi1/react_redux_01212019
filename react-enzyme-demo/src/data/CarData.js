export class CarData {

  constructor(restURL) {
    this._restURL = restURL;
  }

  all() {
    return fetch(`${this._restURL}/cars`).then(res => res.json());
  }

  one(carId) {
    return fetch(`${this._restURL}/cars/${encodeURIComponent(carId)}`).then(res => res.json());
  }

  append(car) {
    return fetch(`${this._restURL}/cars`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(car),
    }).then(res => res.json());
  }

  replace(car) {
    return this.one(car.id).then(oldCar =>
      fetch(`${this._restURL}/cars/${encodeURIComponent(car.id)}`, {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(car),
        }).then(() => oldCar));
  }

  delete(carId) {
    return this.one(carId).then(car =>
      fetch(`${this._restURL}/cars/${encodeURIComponent(carId)}`, {
        method: 'DELETE',
      }).then(() => car));
  }

}

export default CarData;