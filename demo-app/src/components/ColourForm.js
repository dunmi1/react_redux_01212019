import React, { useReducer } from 'react';
import PropTypes from 'prop-types';
 
const colourFormReducer = (state, action) => {

  console.log(state, action);

  switch (action.type) {
    case 'NAME_ENTRY':
      return { ...state, name: action.value };
    case 'HEXCODE_ENTRY':
      return { ...state, hexCode: action.value };
    case 'RESET_FORM':
      return {
        name: '',
        hexCode: '',
      };
    default:
      return state;
  }

};

export const ColourForm = ({ onSubmitColour, buttonText }) => {

  // array destructuring
  // const colourState = useState('');
  const [ colour, dispatch ] = useReducer(colourFormReducer, {
    name: '',
    hexCode: '',
  });

  // console.log('new state: ', colour);

  const submitColour = () => {
    onSubmitColour(colour);
    dispatch({ type: 'RESET_FORM' });
  };

  return <form>
    <div>
      <label htmlFor="name-input">Name:</label>
      <input type="text" id="name-input"
        value={colour.name}
        onChange={e => dispatch({ type: 'NAME_ENTRY', value: e.target.value })} />
    </div>
    <div>
      <label htmlFor="hexcode-input">HexCode:</label>
      <input type="text" id="hexcode-input"
        value={colour.hexCode}
        onChange={e => dispatch({ type: 'HEXCODE_ENTRY', value: e.target.value })} />
    </div>
    <button type="button" onClick={submitColour}>
      {buttonText}
    </button>
  </form>;

};

ColourForm.propTypes = {
  buttonText: PropTypes.string.isRequired,
  onSubmitColour: PropTypes.func.isRequired,
};

ColourForm.defaultProps = {
  buttonText: 'Submit Colour',
};