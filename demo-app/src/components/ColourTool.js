import React from 'react';

import { ToolHeader } from './ToolHeader';
import { ColourForm } from './ColourForm';

import { change } from '../utils';

export class ColourTool extends React.Component {

  state = {
    colours: this.props.colours.slice(),
    someInput: '',
  };

  addColour = (newColour) => {
    this.setState({
      colours: this.state.colours.concat(newColour),
    });
  }

  render() {
    return <>
      <ToolHeader headerText={'Colour Tool' + this.state.someInput} />
      <ul>
        {this.state.colours.map(colour =>
          <li key={colour.name}>
            {colour.name} - {colour.hexCode}
          </li>)}
      </ul>
      Some Input: <input type="text" value={this.state.someInput} onChange={change(this)} name="someInput" />
      <ColourForm 
        onSubmitColour={this.addColour} />
    </>;
  }

};