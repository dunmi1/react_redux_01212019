import { refreshCars } from './refreshCars';

export const DELETE_CAR_REQUEST = '[Cars] Delete Car Request';
export const DELETE_CAR_DONE = '[Cars] Delete Car Done';

export const createDeleteCarRequestAction = carId => ({ type: DELETE_CAR_REQUEST, payload: carId });
export const createDeleteCarDoneAction = car => ({ type: DELETE_CAR_DONE, payload: car });

export const deleteCar = carId => {

  return async (dispatch, getState) => {

    dispatch(createDeleteCarRequestAction(carId));

    const res = await fetch('http://localhost:3050/cars/' + encodeURIComponent(carId));
    const oldCar = await res.json();

    await fetch('http://localhost:3050/cars/' + encodeURIComponent(carId), {
      method: 'DELETE',
    });

    dispatch(createDeleteCarDoneAction(oldCar));

    dispatch(refreshCars());
  };


};