import React from 'react';

import CarViewRowCSS from './CarViewRow.module.css';

export const CarViewRow = ({ car, onEditCar, onDeleteCar }) =>
  <tr>
    <td className={CarViewRowCSS.tableData}>{car.id}</td>
    <td style={{ fontWeight: 'bold' }}>{car.make}</td>
    <td>{car.model}</td>
    <td>{car.year}</td>
    <td>{car.color}</td>
    <td>{car.price}</td>
    <td>
      <button type="button" onClick={() => onEditCar(car.id)}>Edit</button>
      <button type="button" onClick={() => onDeleteCar(car.id)}>Delete</button>
    </td>
  </tr>;
