import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { put, takeEvery, delay } from 'redux-saga/effects';

import { PING, ping, pong, pingReducer } from './redux-common';

function* pingSaga() {
  yield takeEvery(PING, function* action() {
    yield delay(1000);
    yield put(pong());
  });
}

const sagaMiddleware = createSagaMiddleware();

const store = createStore(
  pingReducer,
  applyMiddleware(sagaMiddleware)
);

sagaMiddleware.run(pingSaga);

store.subscribe(() => {
  console.log('saga:', store.getState());
});

store.dispatch(ping());