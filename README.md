# Welcome to React & Redux Class!

## Instructor

Eric Greene

## Schedule

Class:

- Monday - Thursday: 8:30am to 4:30pm

Breaks:

- Morning Break: 10:15am to 10:30am
- Lunch: 12pm to 1pm
- Afternoon Break: 2:45pm to 3:00pm

## Course Outline

- Day 1 - Overview of React, JSX, Function Components, Props, Prop Types, Class Components, State, Event
- Day 2 - Composition, Containment & Specialization, Lifecycle Methods, Using Hooks: State, Reducer and Memoization
- Day 3 - Overview of Redux, Store, Actions, Reducers, Connecting to React using Context, Higher Order Components
- Day 4 - Asynchronous Programming, Callbacks, Promises, Fetch API, Redux-Thunk, Redux-Sagas

## Links

### Instructor's Resources

- [Accelebrate, Inc.](https://www.accelebrate.com/)
- [WintellectNOW](https://www.wintellectnow.com/Home/Instructor?instructorId=EricGreene) - Special Offer Code: GREENE-2016
- [Microsoft Virtual Academy](https://mva.microsoft.com/search/SearchResults.aspx#!q=Eric%20Greene&lang=1033)
- [React Lab](https://github.com/Microsoft/TechnicalCommunityContent/tree/master/Web%20Frameworks/React/Session%202%20-%20Hands%20On)
- [Redux Lab](https://github.com/Microsoft/TechnicalCommunityContent/tree/master/Web%20Frameworks/React/Session%203%20-%20Hands%20On)

### Other Resources

- [You Don't Know JS](https://github.com/getify/You-Dont-Know-JS)
- [JavaScript Air Podcast](http://javascriptair.podbean.com/)
- [Speaking JavaScript](http://speakingjs.com/es5/)
