import { createStore, applyMiddleware } from 'redux';
import { createEpicMiddleware, ofType } from 'redux-observable';
import { delay, mapTo  } from 'rxjs/operators';

import { PING, ping, pong, pingReducer } from './redux-common';

const pingEpic = action$ =>
  action$.pipe(
    ofType(PING),
    delay(1000),
    mapTo(pong()),
  );

const epicMiddleware = createEpicMiddleware();

const store = createStore(
  pingReducer,
  applyMiddleware(epicMiddleware)
);

epicMiddleware.run(pingEpic);

store.subscribe(() => {
  console.log('observable:', store.getState());
});

store.dispatch(ping());

