import React, { useMemo } from 'react';
import { memoize } from 'lodash';
import moize from 'moize';

export const ToolHeader = moize(({ headerText }) => {

  console.log('rendering tool header');
  return <header>
    <h1>{headerText}</h1>
  </header>;
},
{
  isReact: true,
  maxSize: 5,
});

// export const ToolHeader = memoize(({ headerText }) => {

//   console.log('rendering tool header');
//   return <header>
//     <h1>{headerText}</h1>
//   </header>;
// },
// ({ headerText }) => headerText);


// export const ToolHeader = ({ headerText }) => {

//   return useMemo(() => {
//       console.log('rendering tool header');
//       return <header>
//         <h1>{headerText}</h1>
//       </header>;
//     },
//     [headerText]
//   );
// };

// export const ToolHeader = (props) => {
//   return <header>
//     <h1>{props.headerText}</h1>
//   </header>;
// };