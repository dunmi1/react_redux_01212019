import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import {
  createCarAppendAction, createCarCancelAction, createCarDeleteAction,
  createCarEditAction, createCarReplaceAction
} from '../carActions/carActions';

import { refreshCars } from '../carActions/refreshCars';
import { appendCar } from '../carActions/appendCar';
import { replaceCar } from '../carActions/replaceCar';
import { deleteCar } from '../carActions/deleteCar';

import { CarTool } from './CarTool';

export const CarToolContainer = connect(
  ({ cars, editCarId }) => ({ cars, editCarId }),
  dispatch => bindActionCreators({
    onRefreshCars: refreshCars,
    onAppend: appendCar,
    onReplace: replaceCar,
    onDelete: deleteCar,
    onEdit: createCarEditAction,
    onCancel: createCarCancelAction,
  }, dispatch),
)(CarTool);