import { createStore, combineReducers } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';

import { resultReducer as result, historyReducer as history } from './calcReducers';

const calcReducer = combineReducers({
  result, // result: result - shorthand property
  history,
});


export const calcStore = createStore(calcReducer, composeWithDevTools());
