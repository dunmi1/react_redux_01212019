import React from 'react';
import PropTypes from 'prop-types';

export class CarForm extends React.Component {

  static propTypes = {
    buttonText: PropTypes.string.isRequired,
    onSubmitCar: PropTypes.func.isRequired,
  };

  static defaultProps = {
    buttonText: 'Submit Car',
  };  

  state = {
    ...this.initCarForm(),
  };

  initCarForm() {
    return {
      make: '',
      model: '',
      year: 1900,
      color: '',
      price: 0,
    };
  }

  change = ({ target: { name, value, type }}) =>
    this.setState({ [ name ]: type === 'number' ? Number(value) : value });

  submit = () => {
    this.props.onSubmitCar({ ...this.state });
    this.setState(this.initCarForm());
  }

  render() {

    return  <form>
    <div>
      <label htmlFor="make-input">Make:</label>
      <input type="text" id="make-input" value={this.state.make}
        onChange={this.change} name="make" />
    </div>
    <div>
      <label htmlFor="model-input">Model:</label>
      <input type="text" id="model-input" value={this.state.model}
        onChange={this.change} name="model" />
    </div>
    <div>
      <label htmlFor="year-input">Year:</label>
      <input type="number" id="year-input" value={this.state.year}
        onChange={this.change} name="year" />
    </div>
    <div>
      <label htmlFor="color-input">Color:</label>
      <input type="text" id="color-input" value={this.state.color}
        onChange={this.change} name="color" />
    </div>
    <div>
      <label htmlFor="price-input">Price:</label>
      <input type="number" id="price-input" value={this.state.price}
        onChange={this.change} name="price" />
    </div>
    <button type="button" onClick={this.submit}>{this.props.buttonText}</button>
  </form>


  }

}