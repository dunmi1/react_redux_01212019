import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

export const ToDo = props => <li>
  <input type="checkbox" checked={props.todo.completed}
    onChange={() => props.onToggleCompleted(props.todo)} />
  {props.todo.text}
  <button type="button"
    onClick={() => props.onRemove(props.todo.id)}>X</button>
</li>;

export const ToDoForm = props => <input type="text" defaultValue="" onKeyDown={e => {
  if (e.keyCode === 13) {
    e.preventDefault();
    e.stopPropagation();
    props.onSubmitToDo({ text: e.target.value, completed: false });
  }
}} />;

export class ToDos extends React.Component {
  
  componentDidMount() {
    this.props.refreshToDos();
  }

  render() {
    return <div>
      <h2>ToDos</h2>
      <ul>
        {this.props.todos.map(todo => <ToDo todo={todo} key={todo.id}
          onToggleCompleted={this.props.toggleToDoCompleted}
          onRemove={this.props.removeToDo} />)}
      </ul>
      <ToDoForm onSubmitToDo={this.props.addToDo} />
    </div>;
  }

}

export const renderToDos = (connectToDos, store) => {

  const ToDosContainer = connectToDos(ToDos)
  ReactDOM.render(<Provider store={store}>
    <ToDosContainer />
  </Provider>, document.querySelector('#root'));
};