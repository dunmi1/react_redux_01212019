import { createStore, combineReducers, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';

import { carsReducer as cars, editCarIdReducer as editCarId } from './carReducers';

export const carStore = createStore(
  combineReducers({ cars, editCarId }),
  composeWithDevTools(applyMiddleware(thunk)),
);
