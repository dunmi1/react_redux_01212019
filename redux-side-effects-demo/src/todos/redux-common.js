import keyMirror from 'key-mirror';

export const actionTypes = keyMirror({
  REFRESH_TODOS_REQUEST: null,
  REFRESH_TODOS_FULFILLED: null,
  ADD_TODO_REQUEST: null,
  ADD_TODO_FULFILLED: null,
  REMOVE_TODO_REQUEST: null,
  REMOVE_TODO_FULFILLED: null,
  TOGGLE_TODO_COMPLETED_REQUEST: null,
  TOGGLE_TODO_COMPLETED_FULFILLED: null,
});

export const todosReducer = (state = { todos: [] }, action) => {

  switch(action.type) {
    case actionTypes.REFRESH_TODOS_REQUEST:
      return { ...state, pending: true };
    case actionTypes.REFRESH_TODOS_FULFILLED:
      return { ...state, pending: false, todos: action.todos };
    case actionTypes.ADD_TODO_REQUEST:
      return { ...state, pending: true, pendingToDo: action.todo };
    case actionTypes.ADD_TODO_FULFILLED:
      return { ...state, pending: false, fulfilledToDo: action.todo };
    case actionTypes.REMOVE_TODO_REQUEST:
      return { ...state, pending: true, pendingToDo: action.todo };
    case actionTypes.REMOVE_TODO_FULFILLED:
      return { ...state, pending: false, fulfilledToDo: action.todo };
    case actionTypes.TOGGLE_TODO_COMPLETED_REQUEST:
      return { ...state, pending: true, pendingToDo: action.todo };
    case actionTypes.TOGGLE_TODO_COMPLETED_FULFILLED:
      return { ...state, pending: false, fulfilledToDo: action.todo };
    default:
      return state;
  }

};